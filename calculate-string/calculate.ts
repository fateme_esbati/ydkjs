const operators: object = {
    "+": (a: number, b: number) => a + b,
    "-": (a: number, b: number) => a - b,
    "*": (a: number, b: number) => a * b,
    "/": (a: number, b: number) => a / b
}
function calculate(str: string): number {
    const removeSpaces: string[] = str.replace(/\s/g, '').split(/(\+|\-|\*|\/)/);
    let result: number;
    for (let i = 0; i < removeSpaces.length; i++) {
        const node: string = removeSpaces[i];
        const opt: boolean = node in operators;

        if(opt) {
            result = operators[node](Number(removeSpaces[i - 1]), Number(removeSpaces[i + 1]));
            removeSpaces[i+1] = result.toString();
        }
    }
    return result;
}

console.log(calculate("20 + 30 - 10 * 4 / 2"));