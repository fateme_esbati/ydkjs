function calculate(str) {
    var removeSpaces = str.replace(/\s/g, '').split(/(\+|\-|\*|\/)/);
    var operators = {
        "+": function (a, b) { return a + b; },
        "-": function (a, b) { return a - b; },
        "*": function (a, b) { return a * b; },
        "/": function (a, b) { return a / b; }
    };
    var result = 0;
    for (var i = 0; i < removeSpaces.length; i++) {
        var node = removeSpaces[i];
        var opt = node in operators;
        if (opt) {
            result = operators[node](Number(removeSpaces[i - 1]), Number(removeSpaces[i + 1]));
            removeSpaces[i + 1] = result.toString();
        }
    }
    return result;
}
console.log(calculate("20 + 30 - 10 * 4 / 2"));
