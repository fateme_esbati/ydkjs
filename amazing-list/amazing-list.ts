interface month {
    name: string,
    month: number
}
interface year {
    name: string
    year: number
}
interface input {
    name: string
    amount: number
    type: string
}
// let list: year[] | month[] = [];
var create = {
    list: [],
    amazingList(parameter: input): year[] | month[] {
        let obj;
        if(parameter.type === "month"){
            obj = {
                name: parameter.name,
                month: parameter.amount
            }
        }
        else {
            obj = {
                name: parameter.name,
                year: parameter.amount
            }
        }

        create.list.push(obj);
        console.log(create.list)
        return create.list;
    }
}
// Shallow copy

// good ... for adding new arg
let one = {...create};
let tow = {...create};
// one = create
// Object.assign({}, create)

// deep copy --> error using function
// let one = JSON.parse(JSON.stringify(create));
// let tow = JSON.parse(JSON.stringify(create));

console.log(one.amazingList({name: "sara", amount: 5, type: "month"}));
console.log(one.amazingList({name: "ali", amount: 7,type: "month"}));
console.log(tow.amazingList({name: "mitra", amount: 10, type: "year"}));
console.log(one.amazingList({name: "fateme", amount: 2, type: "month"}));
console.log(tow.amazingList({name: "hosein", amount: 3, type: "year"}));




