// a = 21;
// b = a * 2;
// console.log(b);
// alert(b);

// age = prompt( "Please tell me your age:" );
// console.log(age);

// var a = 20;
// a = a + 1;
// a = a * 2;
// console.log(a);

// var a = "42";
// var b = Number(a);
// console.log(a);
// console.log(b);

// var amount = 99.99;
// amount = amount * 2;
// console.log(amount);
// /* convert number to string */
// amount = "$" + String(amount);
// // amount = "$" + amount;
// console.log(amount);

// var TAX_RATE = 0.08;
// var amount = 99.99;
// amount = amount * 2;
// amount = amount + (amount * TAX_RATE);
// console.log(amount);
// console.log(amount.toFixed(2));

// var amount = 99.99;
// // general block // nice
// // {
// //     amount = amount * 2;
// //     console.log(amount);
// // }
// if (amount > 100) {
//     amount = amount * 2;
//     console.log(amount);
// }

// var ACCESSORY_PRICE = 9.99;
//
// var BANK_BALANCE = 302.13;
// var amount = 99.99;
// if(amount < BANK_BALANCE) {
//     console.log("I'll take the accessoty!");
//     amount = amount + ACCESSORY_PRICE;
// }
// else {
//     console.log("No, Thanks.");
// }

// var i = 0;
// while(true) {
//     if((i <= 9) === false) // nice
//         break;
//     console.log(i);
//     i = i + 1;
// }
// for(var i = 0; i <= 9; i++) {
//     console.log(i);
// }

// function printAmount(amt) {
//     console.log(amt.toFixed(2));
// }
// function formatAmount() {
//     return "$" + amount.toFixed(2);
// }
// var amount = 99.99;
// printAmount(amount * 2);
// amount = formatAmount();
// console.log(amount);

// const TAX_RATE = 0.08;
// function calculateFinalPurchaseAmount(amt) {
//     amt = amt + (amt * TAX_RATE);
//     return amt;
// }
// var amount = 99.99;
// amount = calculateFinalPurchaseAmount(amount);
// console.log(amount.toFixed(2));

// function one() {
//     var a = 1;
//     console.log(a);
// }
// function two() {
//     var a = 2;
//     console.log(a);
// }
// one();
// two();

// function outer() {
//     var a = 1;
//     function inner() {
//         var a = 2;
//         console.log(a + a);
//     }
//     inner();
//     console.log(a);
// }
// outer();

/* PRACTICE */
const SPENDING_THRESHOLD = 200;
const TAX_RATE = 0.08;
const PHONE_PRICE = 99.99;
const ACCESSORY_PRICE = 9.99;

var bank_balance;
var amount = 0;

// challenge
bank_balance = prompt( "Please tell me your bank balance:" );
bank_balance = Number(bank_balance);

while (amount < bank_balance) {
    amount = amount + PHONE_PRICE;
    if (amount < SPENDING_THRESHOLD) {
        amount = amount + ACCESSORY_PRICE;
    }
}

function calculateTax(amount) {
    return amount * TAX_RATE;
}

function formatAmount(amount) {
    return "$" + amount.toFixed( 2 );
}

amount = amount + calculateTax( amount );
console.log(
    "Your purchase: " + formatAmount( amount ) + "\n" +
    "your bank balance: " + formatAmount( bank_balance )
);

if (amount > bank_balance) {
    console.log(
        "You can't afford this purchase. :("
    );
}
else
    console.log("You can afford this purchase. good luck!");
