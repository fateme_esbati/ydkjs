function q10() {
    var foo = 5

    function baz() {
        foo = 10 // ?
        console.log(foo)
        return

        function foo() {}

    }
    baz()
    // console.log(baz())
    console.log(foo) // 10
}
q10() // interesting
// wrong answer -> ?