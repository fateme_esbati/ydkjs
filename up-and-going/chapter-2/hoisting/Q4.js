function q4() {
    function foo() {
        var bar = "I'm a bar variable"

        function bar() {
            return "I'm a bar function"
            // console.log("I'm a bar function")
        }
        // wrong answer: ? -> because of string bar, it compiles from top
        // return bar()
    }
    console.log(foo());
}
// q4();