// function q8() {
var x = 'foo';
(function() {
    console.log('x: ' + x) // foo
    var x = 'bar'
    console.log('x: ' + x) // bar
})()
// }
// q8(); // wrong answer -> ? because of inner var x it said undefined