function q5() {
    var counter = 0;
    greeting() // Good evening
    var greeting = function() {
        counter++;
        console.log('Good morning', counter)
    }
    greeting() // Good evening

    function greeting() {
        counter++;
        console.log('Good evening', counter)
    }
    greeting() // Good evening
}
q5(); // wrong answer => understand
// interesting