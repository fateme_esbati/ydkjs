function q2() {
    var foo = 5
    console.log('foo:', foo) // 5
    var foo;
    var bar = 10;
    var bar;
    // wrong answer => understand
    console.log('bar:', bar) // undefined -> 10
    var baz = 10
    var baz = 12
    console.log('baz:', baz) // 12
}
q2();