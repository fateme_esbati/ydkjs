function q3() {
    function foo() {
        return bar()
        // var x = 1;
        // return x;
        // var x = 10;
        // var x = 11;
        function bar() {
            return 5
        }
        function bar() {
            return 10
        }
    }
    // wrong answer: ?
    console.log(foo()); // 5 -> 10
}
q3();