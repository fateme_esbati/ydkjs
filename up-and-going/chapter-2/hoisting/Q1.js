function q1() {
    console.log('bar:', bar) // undefined
    bar = 15 // hoist
    var foo = 1
    console.log(foo, bar) // 1, 15
    var bar;
}
q1();